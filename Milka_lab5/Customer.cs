﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milka_lab5
{
    public partial class Customer_F : Form
    {
        string connectionString = "Server=tcp:itacademy.database.windows.net;" +
            "Database=Milka;" +
            "User Id=Milka;" +
            "Password=Tspi08546;" +
            "Trusted_Connection = False;" +
            "Encrypt = True;";

        public Customer_F()
        {
            InitializeComponent();
        }

        private void Customer_F_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlCommand list = new SqlCommand("Select * From Customer", con);

            SqlDataReader List = list.ExecuteReader();

            namesurname.Items.Clear();
            while(List.Read())
            {
                namesurname.Items.Add(List[1].ToString().Replace(" ", "") + " " + List[2].ToString().Replace(" ", ""));
            }

            con.Close();
        }
        
        private void PrintTable_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlDataAdapter customers = new SqlDataAdapter("Select * From Customer", con);

            DataSet customer_table = new DataSet();

            customers.Fill(customer_table, "Customer");

            dgvCustomer.DataSource = customer_table.Tables["Customer"];
            dgvCustomer.Refresh();

            con.Close();
        }

        private void Add_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            try
            {
                if(name.Text.Replace(" ", "") == "" || surname.Text.Replace(" ", "") == "" || adress.Text.Replace(" ", "") == "")
                {
                    throw new ArgumentException("You don't fill all field");
                }
                SqlCommand add = new SqlCommand("Insert into Customer (Name, Surname, Adress) Values ('" + name.Text.ToString() + "', '" + surname.Text.ToString() + "', '" + adress.Text.ToString() + "')", con);
                add.ExecuteNonQuery();
                MessageBox.Show("Customer add successfully");
            }
            catch (ArgumentException ae)
            {
                MessageBox.Show("Error with add! Enter correctly data. " + ae.Message);
            }
            finally
            {
                Customer_F_Load(sender, e);
                con.Close();
            }
        }

        private void Update_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            try
            {
                if(namesurname.Text.Replace(" ", "") == "")
                {
                    throw new ArgumentException(" Error with manager.");
                }
                String[] name_surname = namesurname.Text.Split();
                if (name_surname.Length != 2)
                {
                    throw new ArgumentException(" Error with manager.");
                }
                if (name_surname[0].Replace(" ", "") == "" || name_surname[1].Replace(" ", "") == "" || newadress.Text.Replace(" ", "") == "")  
                {
                    throw new ArgumentException("You don't fill all field");
                }
                SqlCommand update = new SqlCommand("Update [dbo].[Customer] Set [Adress] = '" + newadress.Text.ToString() +"' where [Name] = '" + name_surname[0] +"' and [Surname] = '" + name_surname[1] +"'", con);
                update.ExecuteNonQuery();
                MessageBox.Show("Customer update successfully");
            }
            catch (ArgumentException ae)
            {
                MessageBox.Show("Error with update. Enter correctly data. " + ae.Message);
            }
            finally
            {
                con.Close();
            }
        }
    }
}
