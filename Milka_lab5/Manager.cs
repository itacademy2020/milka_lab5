﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milka_lab5
{
    public partial class Manager_F :Form
    {
        string connectionString = "Server=tcp:itacademy.database.windows.net;" +
            "Database=Milka;" +
            "User Id=Milka;" +
            "Password=Tspi08546;" +
            "Trusted_Connection = False;" +
            "Encrypt = True;";

        public Manager_F()
        {
            InitializeComponent();
        }

        private void Manager_F_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlCommand list = new SqlCommand("Select * From Manager", con);

            SqlDataReader List = list.ExecuteReader();

            namelist.Items.Clear();
            while (List.Read())
            {
                namelist.Items.Add(List[1].ToString());
            }

            con.Close();
        }

        private void Add_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            try
            {
                if (name.Text.Replace(" ", "") == "" || surname.Text.Replace(" ", "") == "")
                {
                    throw new ArgumentException("You don't fill all field");
                }
                SqlCommand add = new SqlCommand("Insert into Manager (Name, Surname) Values ('" + name.Text.ToString() + "', '" + surname.Text.ToString()  + "')", con);
                add.ExecuteNonQuery();
                MessageBox.Show("Manager add successfully");
            }
            catch (ArgumentException ae)
            {
                MessageBox.Show("Error with add! " + ae.Message);
            }
            finally
            {
                Manager_F_Load(sender, e);
                con.Close();
            }
        }

        private void Update_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            try
            {
                if (namelist.Text.Replace(" ", "") == "" || newsurname.Text.Replace(" ", "") == "")
                {
                    throw new ArgumentException("You don't fill all field");
                }
                SqlCommand update = new SqlCommand("Update [dbo].[Manager] Set [Surname] = '" + newsurname.Text.ToString() + "' where [Name] = '" + namelist.Text.ToString() + "'", con);
                update.ExecuteNonQuery();
                MessageBox.Show("Manager update successfully");
            }
            catch (ArgumentException ae)
            {
                MessageBox.Show("Error with update. " + ae.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private void PrintTable_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlDataAdapter customers = new SqlDataAdapter("Select * From Manager", con);

            DataSet manager_table = new DataSet();

            customers.Fill(manager_table, "Manager");

            dgvManager.DataSource = manager_table.Tables["Manager"];
            dgvManager.Refresh();

            con.Close();
        }
    }
}
