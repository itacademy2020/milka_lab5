﻿namespace Milka_lab5
{
    partial class DeliveryCompany
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.newadress = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.namelist = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.adress = new System.Windows.Forms.TextBox();
            this.name = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvDeliveryCompany = new System.Windows.Forms.DataGridView();
            this.PrintTable = new System.Windows.Forms.Button();
            this.Update = new System.Windows.Forms.Button();
            this.Add = new System.Windows.Forms.Button();
            this.managerlist = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.managerlistnew = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeliveryCompany)).BeginInit();
            this.SuspendLayout();
            // 
            // newadress
            // 
            this.newadress.Location = new System.Drawing.Point(290, 140);
            this.newadress.Name = "newadress";
            this.newadress.Size = new System.Drawing.Size(334, 26);
            this.newadress.TabIndex = 30;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(415, 115);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 20);
            this.label6.TabIndex = 29;
            this.label6.Text = "New adress";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(141, 176);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(585, 20);
            this.label5.TabIndex = 28;
            this.label5.Text = "*For update choose name company and then enter new adress and new manager";
            // 
            // namelist
            // 
            this.namelist.FormattingEnabled = true;
            this.namelist.Location = new System.Drawing.Point(63, 141);
            this.namelist.Name = "namelist";
            this.namelist.Size = new System.Drawing.Size(143, 28);
            this.namelist.TabIndex = 27;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(111, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 20);
            this.label4.TabIndex = 26;
            this.label4.Text = "Name ";
            // 
            // adress
            // 
            this.adress.Location = new System.Drawing.Point(290, 65);
            this.adress.Name = "adress";
            this.adress.Size = new System.Drawing.Size(334, 26);
            this.adress.TabIndex = 25;
            // 
            // name
            // 
            this.name.Location = new System.Drawing.Point(63, 61);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(143, 26);
            this.name.TabIndex = 23;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(415, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 20);
            this.label3.TabIndex = 22;
            this.label3.Text = "Adress";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(101, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 20);
            this.label1.TabIndex = 20;
            this.label1.Text = "Name";
            // 
            // dgvDeliveryCompany
            // 
            this.dgvDeliveryCompany.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDeliveryCompany.Location = new System.Drawing.Point(91, 254);
            this.dgvDeliveryCompany.Name = "dgvDeliveryCompany";
            this.dgvDeliveryCompany.RowHeadersWidth = 62;
            this.dgvDeliveryCompany.RowTemplate.Height = 28;
            this.dgvDeliveryCompany.Size = new System.Drawing.Size(1020, 418);
            this.dgvDeliveryCompany.TabIndex = 19;
            // 
            // PrintTable
            // 
            this.PrintTable.Location = new System.Drawing.Point(530, 199);
            this.PrintTable.Name = "PrintTable";
            this.PrintTable.Size = new System.Drawing.Size(154, 49);
            this.PrintTable.TabIndex = 18;
            this.PrintTable.Text = "Enter table";
            this.PrintTable.UseVisualStyleBackColor = true;
            this.PrintTable.Click += new System.EventHandler(this.PrintTable_Click);
            // 
            // Update
            // 
            this.Update.Location = new System.Drawing.Point(1040, 117);
            this.Update.Name = "Update";
            this.Update.Size = new System.Drawing.Size(154, 49);
            this.Update.TabIndex = 17;
            this.Update.Text = "Update";
            this.Update.UseVisualStyleBackColor = true;
            this.Update.Click += new System.EventHandler(this.Update_Click);
            // 
            // Add
            // 
            this.Add.Location = new System.Drawing.Point(1040, 38);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(154, 49);
            this.Add.TabIndex = 16;
            this.Add.Text = "Add ";
            this.Add.UseVisualStyleBackColor = true;
            this.Add.Click += new System.EventHandler(this.Add_Click);
            // 
            // managerlist
            // 
            this.managerlist.FormattingEnabled = true;
            this.managerlist.Location = new System.Drawing.Point(705, 61);
            this.managerlist.Name = "managerlist";
            this.managerlist.Size = new System.Drawing.Size(192, 28);
            this.managerlist.TabIndex = 31;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(763, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 20);
            this.label2.TabIndex = 32;
            this.label2.Text = "Manager";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(747, 115);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 20);
            this.label7.TabIndex = 33;
            this.label7.Text = "New manager";
            // 
            // managerlistnew
            // 
            this.managerlistnew.BackColor = System.Drawing.Color.White;
            this.managerlistnew.FormattingEnabled = true;
            this.managerlistnew.Location = new System.Drawing.Point(705, 138);
            this.managerlistnew.Name = "managerlistnew";
            this.managerlistnew.Size = new System.Drawing.Size(192, 28);
            this.managerlistnew.TabIndex = 34;
            // 
            // DeliveryCompany
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1225, 713);
            this.Controls.Add(this.managerlistnew);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.managerlist);
            this.Controls.Add(this.newadress);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.namelist);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.adress);
            this.Controls.Add(this.name);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvDeliveryCompany);
            this.Controls.Add(this.PrintTable);
            this.Controls.Add(this.Update);
            this.Controls.Add(this.Add);
            this.Name = "DeliveryCompany";
            this.Text = "DeliveryCompany";
            this.Load += new System.EventHandler(this.DeliveryCompany_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeliveryCompany)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox newadress;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox namelist;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox adress;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvDeliveryCompany;
        private System.Windows.Forms.Button PrintTable;
        private System.Windows.Forms.Button Update;
        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.ComboBox managerlist;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox managerlistnew;
    }
}