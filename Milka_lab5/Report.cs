﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milka_lab5
{
    public partial class Report : Form
    {
        string connectionString = "Server=tcp:itacademy.database.windows.net;" +
            "Database=Milka;" +
            "User Id=Milka;" +
            "Password=Tspi08546;" +
            "Trusted_Connection = False;" +
            "Encrypt = True;";

        public Report()
        {
            InitializeComponent();
        }

       

        private void Report_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            
            SqlCommand listmanager = new SqlCommand("Select * From Manager", con);
            SqlDataReader Listmanager = listmanager.ExecuteReader();
            managerlist.Items.Clear();
            while (Listmanager.Read())
            {
                managerlist.Items.Add(Listmanager[1].ToString().Replace(" ", "") + " " + Listmanager[2].ToString().Replace(" ", ""));
            }
            Listmanager.Close();

            SqlCommand listproduct = new SqlCommand("Select * From Product", con);
            SqlDataReader Listproduct = listproduct.ExecuteReader();
            productlist.Items.Clear();
            while (Listproduct.Read())
            {
                productlist.Items.Add(Listproduct[1].ToString());
            }
            Listproduct.Close();
            con.Close();
        }

        private void select1_Click(object sender, EventArgs e)
        {       
            
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            try
            {
                if (date1.Value >= date2.Value )
                {
                    throw new ArgumentException("Error with date.");
                }

                SqlDataAdapter T = new SqlDataAdapter("Select Name From Product P Where Name Not In ( select P.Name from List L Full Join Request R On L.Request = R.ID Full Join Product P On L.Product = P.ID Where R.OrderDate Between '"+ date1.Value.Date.ToString("yyyy-MM-dd") +"' And '"+ date2.Value.Date.ToString("yyyy-MM-dd") + "')", con);

                DataSet tabl = new DataSet();
                T.Fill(tabl, "Report");
                dgv.DataSource = tabl.Tables["Report"];
                dgv.Refresh();
            }
            catch (ArgumentException ae)
            {
                MessageBox.Show(ae.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            try
            {
                if (productlist.Text.Replace(" ", "") == "")
                {
                    throw new ArgumentException("Enter product list.");
                }

                SqlDataAdapter T = new SqlDataAdapter("select C.Surname, C.Name as 'Customer name', C.Adress, DC.Name as 'Firm name', R.OrderDate from List inner Join Request R On List.Request = R.ID inner Join Product P On List.Product = P.ID Inner Join Customer C On C.ID = R.Customer Inner Join DeliveryCompany DC On DC.ID = R.DeliveryCompany Where P.Name = '"+ productlist.Text +"'", con);

                DataSet tabl = new DataSet();
                T.Fill(tabl, "Report");

                dgv.DataSource = tabl.Tables["Report"];
                dgv.Refresh();
            }
            catch (ArgumentException ae)
            {
                MessageBox.Show(ae.Message);
            }
            con.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            try
            {
                if (chanellist.Text.Replace(" ", "") != "Веб-сайт" || chanellist.Text.Replace(" ", "") != "Додаток")
                {
                    throw new ArgumentException("Enter correctly chanel.");
                }


                if (managerlist.Text.Replace(" ", "") == "")
                {
                    throw new ArgumentException(" Error with manager.");
                }
                String[] name_surname = managerlist.Text.Split();
                if (name_surname.Length != 2)
                {
                    throw new ArgumentException(" Error with manager.");
                }
                if (name_surname[0].Replace(" ", "") == "" || name_surname[1].Replace(" ", "") == "")
                {
                    throw new ArgumentException("You don't fill all field");
                }
                SqlDataAdapter T = new SqlDataAdapter("select R.ID, P.Name, L.Count from List L Full Join Request R On L.Request = R.ID Full Join Product P On L.Product = P.ID Full Join Manager M On M.ID = R.Manager" 
                + " Where M.Surname = '"+ name_surname[1] +"' And M.Name = '"+ name_surname[0] + "' and R.Resource = '"+ chanellist.Text +"'", con);

                DataSet tabl = new DataSet();
                T.Fill(tabl, "Report");

                dgv.DataSource = tabl.Tables["Report"];
                dgv.Refresh();
            }
            catch (ArgumentException ae)
            {
                MessageBox.Show(ae.Message);
            }
            con.Close();
        }
    }
}
