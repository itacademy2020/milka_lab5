﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Milka_lab5
{
    public partial class Products : Form
    {
        string connectionString = "Server=tcp:itacademy.database.windows.net;" +
            "Database=Milka;" +
            "User Id=Milka;" +
            "Password=Tspi08546;" +
            "Trusted_Connection = False;" +
            "Encrypt = True;";

        public Products()
        {
            InitializeComponent();
        }

        private void Add_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            try
            {
                if(categorylist.Text != "Куртки" && categorylist.Text != "Штани" && categorylist.Text != "Взуття")
                {
                    throw new ArgumentException("Error with category.");
                }

                if (managerlist.Text.Replace(" ", "") == "")
                {
                    throw new ArgumentException("Error with manager.");
                }
                String[] name_surname = managerlist.Text.Split();
                if(name_surname.Length != 2)
                {
                    throw new ArgumentException("Error with manager.");
                }

                SqlCommand returnIdManager = new SqlCommand("Select [ID] from [dbo].[Manager] where [Name] = '" + name_surname[0] + "' and [Surname] = '" + name_surname[1] + "'", con);
                SqlDataReader DR = returnIdManager.ExecuteReader();
                int manager = 0;
                while (DR.Read())
                {
                    manager = int.Parse(DR[0].ToString());
                }
                DR.Close();

                if (manager == 0)
                {
                    throw new ArgumentException("Error with manager.");
                }

                if (name.Text.Replace(" ", "") == "" || colorlist.Text.Replace(" ", "") == "" || categorylist.Text.Replace(" ", "") == "")
                {
                    throw new ArgumentException("You don't fill all field");
                }
                int a = 0;
                if(!(int.TryParse(size.Text, out a)))
                {
                    throw new ArgumentException("Write correctly size!");
                }
                if (!(int.TryParse(price.Text, out a)))
                {
                    throw new ArgumentException("Write correctly price!");
                }
                if (int.Parse(price.Text) < 0)
                {
                    throw new ArgumentException("Write correctly price!");
                }
                if (int.Parse(size.Text) > 50)
                {
                    throw new ArgumentException("Write correctly size!");
                }

                SqlCommand add = new SqlCommand("insert into Product (Name, Price, Color, Size, Category, Manager, ChangeDate)" +
                                                "Values('" + name.Text + "', "+ int.Parse(price.Text) + ", '"+ colorlist.Text +"', "+ int.Parse(size.Text) + ", '"+ categorylist.Text.ToString() +"', "+ manager + ", getdate())", con);
                add.ExecuteNonQuery();
                MessageBox.Show("Products add successfully");
            }
            catch (ArgumentException ae)
            {
                MessageBox.Show("Error with add! Enter correctly data. " + ae.Message);
            }
            finally
            {
                Products_Load(sender, e);
                con.Close();
            }
        }

        private void Update_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            try
            {
                int a = 0;
                if (!(int.TryParse(newsize.Text, out a)))
                {
                    throw new ArgumentException("Write correctly size!");
                }
                if (!(int.TryParse(newprice.Text, out a)))
                {
                    throw new ArgumentException("Write correctly price!");
                }
                if (newcategory.Text != "Куртки" && newcategory.Text != "Штани" && newcategory.Text != "Взуття")
                {
                    throw new ArgumentException("Error with category.");
                }
                if (newmanagerlist.Text.Replace(" ", "") == "")
                {
                    throw new ArgumentException("Error with manager.");
                }
                String[] name_surname = newmanagerlist.Text.Split();

                if (name_surname[0].Replace(" ", "") == "" || name_surname[1].Replace(" ", "") == "")
                {
                    throw new ArgumentException("Error with manager.");
                }
                SqlCommand returnIdManager = new SqlCommand("Select [ID] from [dbo].[Manager] where [Name] = '" + name_surname[0] + "' and [Surname] = '" + name_surname[1] + "'", con);
                SqlDataReader DR = returnIdManager.ExecuteReader();
                int manager = 0;
                while (DR.Read())
                {
                    manager = int.Parse(DR[0].ToString());
                }
                DR.Close();

                if (namelist.Text.Replace(" ", "") == "" || newcolorlist.Text.Replace(" ", "") == "" || newcategory.Text.Replace(" ", "") == "")
                {
                    throw new ArgumentException("You don't fill all field");
                }
                if (int.Parse(newprice.Text) < 0)
                {
                    throw new ArgumentException("Write correctly price!");
                }
                if (int.Parse(newsize.Text) > 50)
                {
                    throw new ArgumentException("Write correctly size!");
                }
                if (manager == 0)
                {
                    throw new ArgumentException("Error with manager.");
                }
                SqlCommand update = new SqlCommand("UPDATE [dbo].[Product]" + 
                                                    "SET[Price] = " + int.Parse(newprice.Text) +
                                                        ",[Color] = '"+ newcolorlist.Text +"'"+
                                                        ",[Size] = " + newsize.Text +
                                                        ",[Category] = '"+ newcategory.Text +"'"+
                                                        ",[Manager] = " + manager +
                                                        ",[ChangeDate] = getdate()" +
                                                    "WHERE[Name] = '" + namelist.Text + "'", con);
                update.ExecuteNonQuery();
                MessageBox.Show("Products update successfully");
            }
            catch (ArgumentException ae)
            {
                MessageBox.Show("Error with update. Enter correctly data. " + ae.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private void PrintTable_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlDataAdapter products = new SqlDataAdapter("Select * From Product", con);

            DataSet products_table = new DataSet();

            products.Fill(products_table, "Product");

            dgvProduct.DataSource = products_table.Tables["Product"];
            dgvProduct.Refresh();

            con.Close();
        }

        private void Products_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlCommand listmanager = new SqlCommand("Select * From Manager", con);
            SqlDataReader Listmanager = listmanager.ExecuteReader();
            managerlist.Items.Clear();
            newmanagerlist.Items.Clear();
            while (Listmanager.Read())
            {
                managerlist.Items.Add(Listmanager[1].ToString().Replace(" ", "") + " " + Listmanager[2].ToString().Replace(" ", ""));
                newmanagerlist.Items.Add(Listmanager[1].ToString().Replace(" ", "") + " " + Listmanager[2].ToString().Replace(" ", ""));
            }
            Listmanager.Close();


            SqlCommand list = new SqlCommand("Select * From Product", con);
            SqlDataReader List = list.ExecuteReader();
            namelist.Items.Clear();
            while (List.Read())
            {
                namelist.Items.Add(List[1].ToString());
            }

            List.Close();


            con.Close();
        }
    }
}
