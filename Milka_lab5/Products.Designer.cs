﻿namespace Milka_lab5
{
    partial class Products
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.namelist = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvProduct = new System.Windows.Forms.DataGridView();
            this.PrintTable = new System.Windows.Forms.Button();
            this.Update = new System.Windows.Forms.Button();
            this.Add = new System.Windows.Forms.Button();
            this.price = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.colorlist = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.size = new System.Windows.Forms.TextBox();
            this.categorylist = new System.Windows.Forms.ComboBox();
            this.managerlist = new System.Windows.Forms.ComboBox();
            this.newsize = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.newcolorlist = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.newprice = new System.Windows.Forms.TextBox();
            this.newmanagerlist = new System.Windows.Forms.ComboBox();
            this.newcategory = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduct)).BeginInit();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(169, 150);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(452, 20);
            this.label5.TabIndex = 28;
            this.label5.Text = "*For update choose name surname and then enter new adress";
            // 
            // namelist
            // 
            this.namelist.FormattingEnabled = true;
            this.namelist.Location = new System.Drawing.Point(12, 106);
            this.namelist.Name = "namelist";
            this.namelist.Size = new System.Drawing.Size(116, 28);
            this.namelist.TabIndex = 27;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(54, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 20);
            this.label4.TabIndex = 26;
            this.label4.Text = "Name";
            // 
            // name
            // 
            this.name.Location = new System.Drawing.Point(12, 40);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(116, 26);
            this.name.TabIndex = 23;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(760, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 20);
            this.label3.TabIndex = 22;
            this.label3.Text = "Manager";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(344, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 20);
            this.label2.TabIndex = 21;
            this.label2.Text = "Color";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(54, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 20);
            this.label1.TabIndex = 20;
            this.label1.Text = "Name";
            // 
            // dgvProduct
            // 
            this.dgvProduct.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProduct.Location = new System.Drawing.Point(95, 233);
            this.dgvProduct.Name = "dgvProduct";
            this.dgvProduct.RowHeadersWidth = 62;
            this.dgvProduct.RowTemplate.Height = 28;
            this.dgvProduct.Size = new System.Drawing.Size(1020, 449);
            this.dgvProduct.TabIndex = 19;
            // 
            // PrintTable
            // 
            this.PrintTable.Location = new System.Drawing.Point(512, 178);
            this.PrintTable.Name = "PrintTable";
            this.PrintTable.Size = new System.Drawing.Size(154, 49);
            this.PrintTable.TabIndex = 18;
            this.PrintTable.Text = "Enter table";
            this.PrintTable.UseVisualStyleBackColor = true;
            this.PrintTable.Click += new System.EventHandler(this.PrintTable_Click);
            // 
            // Update
            // 
            this.Update.Location = new System.Drawing.Point(961, 96);
            this.Update.Name = "Update";
            this.Update.Size = new System.Drawing.Size(154, 49);
            this.Update.TabIndex = 17;
            this.Update.Text = "Update";
            this.Update.UseVisualStyleBackColor = true;
            this.Update.Click += new System.EventHandler(this.Update_Click);
            // 
            // Add
            // 
            this.Add.Location = new System.Drawing.Point(961, 17);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(154, 49);
            this.Add.TabIndex = 16;
            this.Add.Text = "Add ";
            this.Add.UseVisualStyleBackColor = true;
            this.Add.Click += new System.EventHandler(this.Add_Click);
            // 
            // price
            // 
            this.price.Location = new System.Drawing.Point(160, 39);
            this.price.Name = "price";
            this.price.Size = new System.Drawing.Size(100, 26);
            this.price.TabIndex = 31;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(189, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 20);
            this.label7.TabIndex = 32;
            this.label7.Text = "Price";
            // 
            // colorlist
            // 
            this.colorlist.FormattingEnabled = true;
            this.colorlist.Items.AddRange(new object[] {
            "Чорний",
            "Синій",
            "Жовтий",
            "Червоний",
            "Білий",
            "Сірий",
            "Зелений",
            "Фіолетовий",
            "Джинсовий"});
            this.colorlist.Location = new System.Drawing.Point(305, 37);
            this.colorlist.Name = "colorlist";
            this.colorlist.Size = new System.Drawing.Size(119, 28);
            this.colorlist.TabIndex = 33;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(465, 17);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 20);
            this.label8.TabIndex = 34;
            this.label8.Text = "Size";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(593, 17);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 20);
            this.label9.TabIndex = 35;
            this.label9.Text = "Category";
            // 
            // size
            // 
            this.size.Location = new System.Drawing.Point(452, 41);
            this.size.Name = "size";
            this.size.Size = new System.Drawing.Size(65, 26);
            this.size.TabIndex = 36;
            // 
            // categorylist
            // 
            this.categorylist.FormattingEnabled = true;
            this.categorylist.Items.AddRange(new object[] {
            "Куртки",
            "Штани",
            "Взуття"});
            this.categorylist.Location = new System.Drawing.Point(565, 39);
            this.categorylist.Name = "categorylist";
            this.categorylist.Size = new System.Drawing.Size(119, 28);
            this.categorylist.TabIndex = 37;
            // 
            // managerlist
            // 
            this.managerlist.FormattingEnabled = true;
            this.managerlist.Location = new System.Drawing.Point(746, 38);
            this.managerlist.Name = "managerlist";
            this.managerlist.Size = new System.Drawing.Size(121, 28);
            this.managerlist.TabIndex = 38;
            // 
            // newsize
            // 
            this.newsize.Location = new System.Drawing.Point(452, 108);
            this.newsize.Name = "newsize";
            this.newsize.Size = new System.Drawing.Size(65, 26);
            this.newsize.TabIndex = 40;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(448, 85);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 20);
            this.label6.TabIndex = 39;
            this.label6.Text = "New size";
            // 
            // newcolorlist
            // 
            this.newcolorlist.FormattingEnabled = true;
            this.newcolorlist.Items.AddRange(new object[] {
            "Чорний",
            "Синій",
            "Жовтий",
            "Червоний",
            "Білий",
            "Сірий",
            "Зелений",
            "Фіолетовий",
            "Джинсовий"});
            this.newcolorlist.Location = new System.Drawing.Point(305, 106);
            this.newcolorlist.Name = "newcolorlist";
            this.newcolorlist.Size = new System.Drawing.Size(119, 28);
            this.newcolorlist.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(327, 85);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 20);
            this.label10.TabIndex = 41;
            this.label10.Text = "New Color";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(169, 85);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(78, 20);
            this.label11.TabIndex = 44;
            this.label11.Text = "New price";
            // 
            // newprice
            // 
            this.newprice.Location = new System.Drawing.Point(160, 108);
            this.newprice.Name = "newprice";
            this.newprice.Size = new System.Drawing.Size(100, 26);
            this.newprice.TabIndex = 43;
            // 
            // newmanagerlist
            // 
            this.newmanagerlist.FormattingEnabled = true;
            this.newmanagerlist.Location = new System.Drawing.Point(746, 106);
            this.newmanagerlist.Name = "newmanagerlist";
            this.newmanagerlist.Size = new System.Drawing.Size(121, 28);
            this.newmanagerlist.TabIndex = 48;
            // 
            // newcategory
            // 
            this.newcategory.FormattingEnabled = true;
            this.newcategory.Items.AddRange(new object[] {
            "Куртки",
            "Штани",
            "Взуття"});
            this.newcategory.Location = new System.Drawing.Point(565, 107);
            this.newcategory.Name = "newcategory";
            this.newcategory.Size = new System.Drawing.Size(119, 28);
            this.newcategory.TabIndex = 47;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(561, 84);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(105, 20);
            this.label12.TabIndex = 46;
            this.label12.Text = "New category";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(742, 85);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(107, 20);
            this.label13.TabIndex = 45;
            this.label13.Text = "New manager";
            // 
            // Products
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1207, 714);
            this.Controls.Add(this.newmanagerlist);
            this.Controls.Add(this.newcategory);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.newprice);
            this.Controls.Add(this.newcolorlist);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.newsize);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.managerlist);
            this.Controls.Add(this.categorylist);
            this.Controls.Add(this.size);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.colorlist);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.price);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.namelist);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.name);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvProduct);
            this.Controls.Add(this.PrintTable);
            this.Controls.Add(this.Update);
            this.Controls.Add(this.Add);
            this.Name = "Products";
            this.Text = "Products";
            this.Load += new System.EventHandler(this.Products_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduct)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox namelist;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvProduct;
        private System.Windows.Forms.Button PrintTable;
        private System.Windows.Forms.Button Update;
        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.TextBox price;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox colorlist;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox size;
        private System.Windows.Forms.ComboBox categorylist;
        private System.Windows.Forms.ComboBox managerlist;
        private System.Windows.Forms.TextBox newsize;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox newcolorlist;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox newprice;
        private System.Windows.Forms.ComboBox newmanagerlist;
        private System.Windows.Forms.ComboBox newcategory;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
    }
}