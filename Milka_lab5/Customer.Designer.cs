﻿namespace Milka_lab5
{
    partial class Customer_F
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Add = new System.Windows.Forms.Button();
            this.Update = new System.Windows.Forms.Button();
            this.PrintTable = new System.Windows.Forms.Button();
            this.dgvCustomer = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.TextBox();
            this.surname = new System.Windows.Forms.TextBox();
            this.adress = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.namesurname = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.newadress = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomer)).BeginInit();
            this.SuspendLayout();
            // 
            // Add
            // 
            this.Add.Location = new System.Drawing.Point(926, 23);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(154, 49);
            this.Add.TabIndex = 0;
            this.Add.Text = "Add ";
            this.Add.UseVisualStyleBackColor = true;
            this.Add.Click += new System.EventHandler(this.Add_Click);
            // 
            // Update
            // 
            this.Update.Location = new System.Drawing.Point(926, 102);
            this.Update.Name = "Update";
            this.Update.Size = new System.Drawing.Size(154, 49);
            this.Update.TabIndex = 1;
            this.Update.Text = "Update";
            this.Update.UseVisualStyleBackColor = true;
            this.Update.Click += new System.EventHandler(this.Update_Click);
            // 
            // PrintTable
            // 
            this.PrintTable.Location = new System.Drawing.Point(477, 184);
            this.PrintTable.Name = "PrintTable";
            this.PrintTable.Size = new System.Drawing.Size(154, 49);
            this.PrintTable.TabIndex = 2;
            this.PrintTable.Text = "Enter table";
            this.PrintTable.UseVisualStyleBackColor = true;
            this.PrintTable.Click += new System.EventHandler(this.PrintTable_Click);
            // 
            // dgvCustomer
            // 
            this.dgvCustomer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCustomer.Location = new System.Drawing.Point(60, 239);
            this.dgvCustomer.Name = "dgvCustomer";
            this.dgvCustomer.RowHeadersWidth = 62;
            this.dgvCustomer.RowTemplate.Height = 28;
            this.dgvCustomer.Size = new System.Drawing.Size(1020, 449);
            this.dgvCustomer.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(98, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(305, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Surname";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(632, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Adress";
            // 
            // name
            // 
            this.name.Location = new System.Drawing.Point(48, 46);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(143, 26);
            this.name.TabIndex = 7;
            // 
            // surname
            // 
            this.surname.Location = new System.Drawing.Point(260, 46);
            this.surname.Name = "surname";
            this.surname.Size = new System.Drawing.Size(168, 26);
            this.surname.TabIndex = 9;
            // 
            // adress
            // 
            this.adress.Location = new System.Drawing.Point(490, 46);
            this.adress.Name = "adress";
            this.adress.Size = new System.Drawing.Size(377, 26);
            this.adress.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(182, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 20);
            this.label4.TabIndex = 11;
            this.label4.Text = "Name Surname";
            // 
            // namesurname
            // 
            this.namesurname.FormattingEnabled = true;
            this.namesurname.Location = new System.Drawing.Point(125, 123);
            this.namesurname.Name = "namesurname";
            this.namesurname.Size = new System.Drawing.Size(264, 28);
            this.namesurname.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(134, 156);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(452, 20);
            this.label5.TabIndex = 13;
            this.label5.Text = "*For update choose name surname and then enter new adress";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(632, 102);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 20);
            this.label6.TabIndex = 14;
            this.label6.Text = "New adress";
            // 
            // newadress
            // 
            this.newadress.Location = new System.Drawing.Point(490, 126);
            this.newadress.Name = "newadress";
            this.newadress.Size = new System.Drawing.Size(377, 26);
            this.newadress.TabIndex = 15;
            // 
            // Customer_F
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1146, 809);
            this.Controls.Add(this.newadress);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.namesurname);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.adress);
            this.Controls.Add(this.surname);
            this.Controls.Add(this.name);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvCustomer);
            this.Controls.Add(this.PrintTable);
            this.Controls.Add(this.Update);
            this.Controls.Add(this.Add);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1168, 865);
            this.Name = "Customer_F";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Customer";
            this.Load += new System.EventHandler(this.Customer_F_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.Button Update;
        private System.Windows.Forms.Button PrintTable;
        private System.Windows.Forms.DataGridView dgvCustomer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.TextBox surname;
        private System.Windows.Forms.TextBox adress;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox namesurname;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox newadress;
    }
}