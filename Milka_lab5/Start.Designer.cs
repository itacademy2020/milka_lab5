﻿namespace Milka_lab5
{
    partial class Start
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Start));
            this.Customer_B = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.DeliveryCompany_B = new System.Windows.Forms.Button();
            this.Product_B = new System.Windows.Forms.Button();
            this.Manager_B = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Report1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Customer_B
            // 
            this.Customer_B.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.Customer_B.Location = new System.Drawing.Point(76, 62);
            this.Customer_B.Name = "Customer_B";
            this.Customer_B.Size = new System.Drawing.Size(119, 61);
            this.Customer_B.TabIndex = 0;
            this.Customer_B.Text = "Customer";
            this.Customer_B.UseVisualStyleBackColor = false;
            this.Customer_B.Click += new System.EventHandler(this.Customer_B_Click);
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.Report1);
            this.panel1.Controls.Add(this.DeliveryCompany_B);
            this.panel1.Controls.Add(this.Product_B);
            this.panel1.Controls.Add(this.Manager_B);
            this.panel1.Controls.Add(this.Customer_B);
            this.panel1.Location = new System.Drawing.Point(126, 91);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(573, 394);
            this.panel1.TabIndex = 1;
            // 
            // DeliveryCompany_B
            // 
            this.DeliveryCompany_B.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.DeliveryCompany_B.Location = new System.Drawing.Point(345, 164);
            this.DeliveryCompany_B.Name = "DeliveryCompany_B";
            this.DeliveryCompany_B.Size = new System.Drawing.Size(119, 61);
            this.DeliveryCompany_B.TabIndex = 3;
            this.DeliveryCompany_B.Text = "Delivery Company";
            this.DeliveryCompany_B.UseVisualStyleBackColor = false;
            this.DeliveryCompany_B.Click += new System.EventHandler(this.DeliveryCompany_B_Click);
            // 
            // Product_B
            // 
            this.Product_B.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.Product_B.Location = new System.Drawing.Point(76, 164);
            this.Product_B.Name = "Product_B";
            this.Product_B.Size = new System.Drawing.Size(119, 61);
            this.Product_B.TabIndex = 2;
            this.Product_B.Text = "Product";
            this.Product_B.UseVisualStyleBackColor = false;
            this.Product_B.Click += new System.EventHandler(this.Product_B_Click);
            // 
            // Manager_B
            // 
            this.Manager_B.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.Manager_B.Location = new System.Drawing.Point(345, 62);
            this.Manager_B.Name = "Manager_B";
            this.Manager_B.Size = new System.Drawing.Size(119, 61);
            this.Manager_B.TabIndex = 1;
            this.Manager_B.Text = "Manager";
            this.Manager_B.UseVisualStyleBackColor = false;
            this.Manager_B.Click += new System.EventHandler(this.Manager_B_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(96, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(656, 32);
            this.label1.TabIndex = 2;
            this.label1.Text = " Click on the button to work with the table or reports";
            // 
            // Report1
            // 
            this.Report1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.Report1.Location = new System.Drawing.Point(199, 257);
            this.Report1.Name = "Report1";
            this.Report1.Size = new System.Drawing.Size(119, 61);
            this.Report1.TabIndex = 1;
            this.Report1.Text = "Reports";
            this.Report1.UseVisualStyleBackColor = false;
            this.Report1.Click += new System.EventHandler(this.Report1_Click);
            // 
            // Start
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(800, 532);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Start";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lab_5";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Customer_B;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button Manager_B;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button DeliveryCompany_B;
        private System.Windows.Forms.Button Product_B;
        private System.Windows.Forms.Button Report1;
    }
}

