﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milka_lab5
{
    public partial class Start : Form
    {
        public Start()
        {
            InitializeComponent();
        }
        
        private void Customer_B_Click(object sender, EventArgs e)
        {
            Customer_F form = new Customer_F();
            form.Show();
        }
        private void Manager_B_Click(object sender, EventArgs e)
        {
            Manager_F form = new Manager_F();
            form.Show();
        }

        private void Product_B_Click(object sender, EventArgs e)
        {
            Products form = new Products();
            form.Show();
        }

        private void DeliveryCompany_B_Click(object sender, EventArgs e)
        {
            DeliveryCompany form = new DeliveryCompany();
            form.Show();
        }


        private void Report1_Click(object sender, EventArgs e)
        {
            Report form = new Report();
            form.Show();
        }

        
    }
}
