﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milka_lab5
{
    public partial class DeliveryCompany : Form
    {
        string connectionString = "Server=tcp:itacademy.database.windows.net;" +
            "Database=Milka;" +
            "User Id=Milka;" +
            "Password=Tspi08546;" +
            "Trusted_Connection = False;" +
            "Encrypt = True;";

        public DeliveryCompany()
        {
            InitializeComponent();
        }


        private void Add_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            try
            {
                if (managerlist.Text.Replace(" ", "") == "")
                {
                    throw new ArgumentException(" Error with manager.");
                }
                String[] name_surname = managerlist.Text.Split();
                if (name_surname.Length != 2)
                {
                    throw new ArgumentException(" Error with manager.");
                }
                if (name_surname[0].Replace(" ", "") == "" || name_surname[1].Replace(" ", "") == "")
                {
                    throw new ArgumentException("You don't fill all field");
                }
                SqlCommand returnIdManager = new SqlCommand("Select [ID] from [dbo].[Manager] where [Name] = '" + name_surname[0] + "' and [Surname] = '" + name_surname[1] + "'", con);

                SqlDataReader DR = returnIdManager.ExecuteReader();
                int manager = 0;

                while (DR.Read())
                {
                    manager = int.Parse(DR[0].ToString());
                }
                DR.Close();

                if(manager == 0)
                {
                    throw new ArgumentException("Enter correctly manager.");
                }
                if (name.Text.Replace(" ", "") == "" || adress.Text.Replace(" ", "") == "")
                {
                    throw new ArgumentException("You don't fill all field");
                }

                SqlCommand add = new SqlCommand("Insert into DeliveryCompany (Name, Adress, Manager, ChangeDate) Values ('" + name.Text.ToString() + "', '" + adress.Text.ToString() + "', " + (int)manager + ", getdate())", con);
                add.ExecuteNonQuery();
                MessageBox.Show("Delivery company add successfully");
            }
            catch (ArgumentException ae)
            {
                MessageBox.Show("Error with add! Enter correctly data. " + ae.Message);
            }
            finally
            {
                DeliveryCompany_Load(sender, e);
                con.Close();
            }
        }

        private void Update_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            try
            {
                if (managerlist.Text.Replace(" ", "") == "")
                {
                    throw new ArgumentException(" Error with manager.");
                }
                String[] name_surname = managerlistnew.Text.Split();
                if (name_surname.Length != 2)
                {
                    throw new ArgumentException(" Error with manager.");
                }
                if (name_surname[0].Replace(" ", "") == "" || name_surname[1].Replace(" ", "") == "")
                {
                    throw new ArgumentException("You don't fill all field");
                }

                SqlCommand returnIdManager = new SqlCommand("Select [ID] from [dbo].[Manager] where [Name] = '" + name_surname[0] + "' and [Surname] = '" + name_surname[1] + "'", con);

                SqlDataReader DR = returnIdManager.ExecuteReader();
                int manager = 0;

                while (DR.Read())
                {
                    manager = int.Parse(DR[0].ToString());
                }
                DR.Close();

                if (manager == 0)
                {
                    throw new ArgumentException("Enter correctly manager.");
                }
                String[] name = namelist.Text.Split();

                if (name[0].Replace(" ", "") == "" || newadress.Text.Replace(" ", "") == "")
                {
                    throw new ArgumentException("You don't fill all field");
                }

                SqlCommand update = new SqlCommand("Update [dbo].[DeliveryCompany] Set Adress = '"+ newadress.Text.ToString() +"', Manager = " + manager +", ChangeDate = getDate() Where Name = '" + name[0] + "'", con);
                update.ExecuteNonQuery();
                MessageBox.Show("Customer update successfully");
            }
            catch (ArgumentException ae)
            {
                MessageBox.Show("Error with update. Enter correctly data. " + ae.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private void DeliveryCompany_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlCommand list = new SqlCommand("Select * From DeliveryCompany", con);

            SqlDataReader List = list.ExecuteReader();

            namelist.Items.Clear();
            while (List.Read())
            {
                namelist.Items.Add(List[1].ToString());
            }

            List.Close();

            SqlCommand listmanager = new SqlCommand("Select * From Manager", con);

            SqlDataReader Listmanager = listmanager.ExecuteReader();

            managerlist.Items.Clear();
            managerlistnew.Items.Clear();
            while (Listmanager.Read())
            {
                managerlist.Items.Add(Listmanager[1].ToString().Replace(" ", "") + " " + Listmanager[2].ToString().Replace(" ", ""));
                managerlistnew.Items.Add(Listmanager[1].ToString().Replace(" ", "") + " " + Listmanager[2].ToString().Replace(" ", ""));
            }

            con.Close();
        }

        private void PrintTable_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlDataAdapter delivery_company = new SqlDataAdapter("Select * From DeliveryCompany", con);

            DataSet dc_table = new DataSet();

            delivery_company.Fill(dc_table, "DeliveryCompany");

            dgvDeliveryCompany.DataSource = dc_table.Tables["DeliveryCompany"];
            dgvDeliveryCompany.Refresh();

            con.Close();
        }

    }
}
