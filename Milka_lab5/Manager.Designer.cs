﻿namespace Milka_lab5
{
    partial class Manager_F
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.newsurname = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.namelist = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.surname = new System.Windows.Forms.TextBox();
            this.name = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Update = new System.Windows.Forms.Button();
            this.Add = new System.Windows.Forms.Button();
            this.PrintTable = new System.Windows.Forms.Button();
            this.dgvManager = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvManager)).BeginInit();
            this.SuspendLayout();
            // 
            // newsurname
            // 
            this.newsurname.Location = new System.Drawing.Point(522, 132);
            this.newsurname.Name = "newsurname";
            this.newsurname.Size = new System.Drawing.Size(377, 26);
            this.newsurname.TabIndex = 28;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(664, 108);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(106, 20);
            this.label6.TabIndex = 27;
            this.label6.Text = "New surname";
            // 
            // namelist
            // 
            this.namelist.FormattingEnabled = true;
            this.namelist.Location = new System.Drawing.Point(206, 132);
            this.namelist.Name = "namelist";
            this.namelist.Size = new System.Drawing.Size(264, 28);
            this.namelist.TabIndex = 25;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(301, 106);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 20);
            this.label4.TabIndex = 24;
            this.label4.Text = "Name ";
            // 
            // surname
            // 
            this.surname.Location = new System.Drawing.Point(651, 52);
            this.surname.Name = "surname";
            this.surname.Size = new System.Drawing.Size(168, 26);
            this.surname.TabIndex = 22;
            // 
            // name
            // 
            this.name.Location = new System.Drawing.Point(267, 52);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(143, 26);
            this.name.TabIndex = 21;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(700, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 20);
            this.label2.TabIndex = 19;
            this.label2.Text = "Surname";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(313, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 20);
            this.label1.TabIndex = 18;
            this.label1.Text = "Name";
            // 
            // Update
            // 
            this.Update.Location = new System.Drawing.Point(958, 108);
            this.Update.Name = "Update";
            this.Update.Size = new System.Drawing.Size(154, 49);
            this.Update.TabIndex = 17;
            this.Update.Text = "Update";
            this.Update.UseVisualStyleBackColor = true;
            this.Update.Click += new System.EventHandler(this.Update_Click);
            // 
            // Add
            // 
            this.Add.Location = new System.Drawing.Point(958, 29);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(154, 49);
            this.Add.TabIndex = 16;
            this.Add.Text = "Add ";
            this.Add.UseVisualStyleBackColor = true;
            this.Add.Click += new System.EventHandler(this.Add_Click);
            // 
            // PrintTable
            // 
            this.PrintTable.Location = new System.Drawing.Point(526, 185);
            this.PrintTable.Name = "PrintTable";
            this.PrintTable.Size = new System.Drawing.Size(154, 49);
            this.PrintTable.TabIndex = 29;
            this.PrintTable.Text = "Enter table";
            this.PrintTable.UseVisualStyleBackColor = true;
            this.PrintTable.Click += new System.EventHandler(this.PrintTable_Click);
            // 
            // dgvManager
            // 
            this.dgvManager.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvManager.Location = new System.Drawing.Point(109, 240);
            this.dgvManager.Name = "dgvManager";
            this.dgvManager.RowHeadersWidth = 62;
            this.dgvManager.RowTemplate.Height = 28;
            this.dgvManager.Size = new System.Drawing.Size(1020, 423);
            this.dgvManager.TabIndex = 30;
            // 
            // Manager_F
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1204, 689);
            this.Controls.Add(this.dgvManager);
            this.Controls.Add(this.PrintTable);
            this.Controls.Add(this.newsurname);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.namelist);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.surname);
            this.Controls.Add(this.name);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Update);
            this.Controls.Add(this.Add);
            this.Name = "Manager_F";
            this.Text = "Manager";
            this.Load += new System.EventHandler(this.Manager_F_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvManager)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox newsurname;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox namelist;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox surname;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Update;
        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.Button PrintTable;
        private System.Windows.Forms.DataGridView dgvManager;
    }
}